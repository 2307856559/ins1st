package com.ins1st.modules.gen.service;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.ins1st.config.GenConfig;
import com.ins1st.core.Page;
import com.ins1st.model.GenEntity;
import com.ins1st.util.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("genService")
public class GenService {

    private static final Logger log = LoggerFactory.getLogger(GenService.class);

    @Autowired
    private GenConfig genConfig;

    public void gen(GenEntity genEntity) {
        genConfig.start(genEntity);
    }







}
