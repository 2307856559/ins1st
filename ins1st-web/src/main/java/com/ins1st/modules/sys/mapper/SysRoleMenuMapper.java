package com.ins1st.modules.sys.mapper;

import com.ins1st.modules.sys.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色和菜单表 Mapper 接口
 * </p>
 *
 * @author sun
 * @since 2019-05-09
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
