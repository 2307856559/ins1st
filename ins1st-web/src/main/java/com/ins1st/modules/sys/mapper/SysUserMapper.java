package com.ins1st.modules.sys.mapper;

import com.ins1st.modules.sys.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author sun
 * @since 2019-05-09
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
