package com.ins1st.modules.sys.service;

import com.ins1st.modules.sys.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author sun
 * @since 2019-05-14
 */
public interface ISysDictService extends IService<SysDict> {

}
