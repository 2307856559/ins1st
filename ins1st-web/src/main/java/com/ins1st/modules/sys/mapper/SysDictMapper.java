package com.ins1st.modules.sys.mapper;

import com.ins1st.modules.sys.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author sun
 * @since 2019-05-14
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
