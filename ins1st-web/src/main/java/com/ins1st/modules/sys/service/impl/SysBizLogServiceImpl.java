package com.ins1st.modules.sys.service.impl;

import com.ins1st.modules.sys.entity.SysBizLog;
import com.ins1st.modules.sys.mapper.SysBizLogMapper;
import com.ins1st.modules.sys.service.ISysBizLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 业务日志 服务实现类
 * </p>
 *
 * @author sun
 * @since 2019-05-10
 */
@Service("sysBizLogService")
@Transactional
public class SysBizLogServiceImpl extends ServiceImpl<SysBizLogMapper, SysBizLog> implements ISysBizLogService {

}
