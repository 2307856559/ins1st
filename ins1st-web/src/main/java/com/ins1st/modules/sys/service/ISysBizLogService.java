package com.ins1st.modules.sys.service;

import com.ins1st.modules.sys.entity.SysBizLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 业务日志 服务类
 * </p>
 *
 * @author sun
 * @since 2019-05-10
 */
public interface ISysBizLogService extends IService<SysBizLog> {

}
