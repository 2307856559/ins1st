package com.ins1st;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class
})
@MapperScan("com.ins1st")
@EnableTransactionManagement
public class Ins1stWebApplication {


    public static void main(String[] args) {
        SpringApplication.run(Ins1stWebApplication.class, args);
    }

}
