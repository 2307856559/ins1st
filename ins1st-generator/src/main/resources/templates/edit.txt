@layout("/common/head.html"){
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>表单</legend>
</fieldset>
<form class="layui-form" action="">
        <%for(field in map["columns"]){
            println('<#input inputName="'+field.comment+'" id="'+field.propertyName+'" name="'+field.propertyName+'" value="${'+field.propertyName+'}" required="required"/>');
        }%>
        <%
            print('<#button btName="添加" filter="edit"/>');
        %>
</form>
<script src="\${ctxPath}/static/modules/${map["ge"].moduleName}/${map["ge"].bizName}/${map["ge"].bizName}_edit.js"></script>
</body>
@}